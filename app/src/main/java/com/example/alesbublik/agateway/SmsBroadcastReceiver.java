package com.example.alesbublik.agateway;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by alesbublik on 12/26/17.
 */


public class SmsBroadcastReceiver extends BroadcastReceiver {
    SmsManager smsManager = SmsManager.getDefault();
    public static final String SMS_BUNDLE = "pdus";

    public void onReceive(Context context, Intent intent) {
        Bundle intentExtras = intent.getExtras();
        String serviceProviderSmsCondition = "sdh ";
        String serviceProviderNumber = "+420775916375";
        ArrayList<String> receiversToSend = new ArrayList<>();
        receiversToSend.add("+420775916375");


        if (intentExtras != null) {
            Object[] sms = (Object[]) intentExtras.get(SMS_BUNDLE);
            String smsMessageStr = "";

            for (int i = 0; i < sms.length; ++i) {
                String format = intentExtras.getString("format");
                SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) sms[i], format);
                String smsSender = smsMessage.getOriginatingAddress();

                String smsBody = smsMessage.getMessageBody().toString();
                String address = smsMessage.getOriginatingAddress();

                Log.i("OnReceive", smsBody);
                Log.i("sender", smsSender);
                if (smsSender.equals(serviceProviderNumber) && smsBody.toLowerCase().startsWith(serviceProviderSmsCondition)) {
                    for (int k = 0; k < receiversToSend.size(); k++) {
                        smsManager.sendTextMessage(receiversToSend.get(i), null, smsBody, null, null);
                        switch(getResultCode()) {
                            case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                                Toast.makeText(context, "Generic failure cause", Toast.LENGTH_SHORT).show();
                                break;
                            case SmsManager.RESULT_ERROR_NO_SERVICE:
                                Toast.makeText(context, "Service is currently unavailable", Toast.LENGTH_SHORT).show();
                                break;
                            case SmsManager.RESULT_ERROR_NULL_PDU:
                                Toast.makeText(context, "No pdu provided", Toast.LENGTH_SHORT).show();
                                break;
                            case SmsManager.RESULT_ERROR_RADIO_OFF:
                                Toast.makeText(context, "Radio was explicitly turned off", Toast.LENGTH_SHORT).show();
                                break;
                        }

                    }
                }

                smsMessageStr += "SMS From: " + address + "\n";
                smsMessageStr += smsBody + "\n";
            }
            Toast.makeText(context, "Message Received!", Toast.LENGTH_SHORT).show();
            if (MainActivity.active) {
                MainActivity inst = MainActivity.instance();
                inst.updateInbox(smsMessageStr);
            } else {
                Intent i = new Intent(context, MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);

            }
        }
    }
}